# Programming

* Simplicity over Complexity
* **Simple** first, then **Easy**
* Abstraction, Abstraction, Abstraction
* Put your conventions layer on top of a well written abstraction
* Always choose a name for variables, keys and any namable data which implies the content or usage.
* Never ever use variable names such as `x`, `a`, `b`, etc.
* Bugs before new features
* Docs before new features
* Dedicate each PR/MR to an specific task. Don't clean up or do multiple tasks in the same PR/MR.
* Dedicate each commit to an specific sub task.

## New Feature Checklist

* Is it easy to extend the feature ?
* Is it scalable ?
* Is it easy to maintan it ?
* Is it well documented ?
* Does it have the best possible performance ?
* What about tests ?
* Is it following the correct coding style ?
